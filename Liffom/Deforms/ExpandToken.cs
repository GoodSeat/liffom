﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodSeat.Liffom.Deforms
{
    /// <summary>
    /// 展開変形を表す変形識別トークンを表します。
    /// </summary>
    [Serializable()]
    public class ExpandToken : DeformToken
    {
    }

}
