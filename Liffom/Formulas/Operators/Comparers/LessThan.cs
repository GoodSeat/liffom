﻿using System;
using System.Collections.Generic;
using System.Text;
using GoodSeat.Liffom.Deforms;
using GoodSeat.Liffom.Formulas.Units;

namespace GoodSeat.Liffom.Formulas.Operators.Comparers
{
    /// <summary>
    /// 不等号(より小)による数式の比較を表します。
    /// </summary>
    [Serializable()]
    public class LessThan : Comparer
    {
        bool _containEqual = false;

        /// <summary>
        /// 包含比較か否かを設定もしくは取得します。
        /// </summary>
        public bool ContainEqual { get { return _containEqual; } set { _containEqual = value; } }

        /// <summary>
        /// 不等号(より小)による数式の比較を初期化します。
        /// </summary>
        /// <param name="lhs">左辺。</param>
        /// <param name="rhs">右辺。</param>
        public LessThan(Formula lhs, Formula rhs)
            : base(lhs, rhs)
        { }

        /// <summary>
        /// 不等号(より小)による数式の比較を初期化します。
        /// </summary>
        /// <param name="lhs">左辺。</param>
        /// <param name="rhs">右辺。</param>
        /// <param name="containEqual">包含比較か否か。</param>
        public LessThan(Formula lhs, Formula rhs, bool containEqual)
            : base(lhs, rhs)
        { ContainEqual = containEqual; }

        /// <summary>
        /// 引数を指定して、演算を生成します。
        /// </summary>
        /// <param name="args">初期化に用いるか変数の数式。</param>
        public override Operator CreateOperator(params Formula[] args)
        {
            return new LessThan(args[0], args[1], ContainEqual);
        }

        public override string GetText()
        {
            string mark = ContainEqual ? "≦" : "<";
            return LeftHandSide.ToString() + mark + RightHandSide.ToString();
        }


        public override Judge GetJudge(DeformToken token)
        {
            Formula f = (LeftHandSide - RightHandSide).DeformFormula(token);
            f = f.ClearUnit();

            if (f is Numeric)
            {
                if (ContainEqual)
                {
                    if ((f as Numeric).Figure <= 0) return Judge.True;
                    else return Judge.False;
                }
                else
                {
                    if ((f as Numeric).Figure < 0) return Judge.True;
                    else return Judge.False;
                }
            }
            else
                return Judge.None;
        }

    }
}
