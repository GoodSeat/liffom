using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing.Drawing2D;
using System.Drawing;
using GoodSeat.Liffom.Deforms;
using GoodSeat.Liffom.Deforms.Rules;
using GoodSeat.Liffom.Formulas.Functions.Rules;
using GoodSeat.Liffom.Formulas.Operators;

namespace GoodSeat.Liffom.Formulas.Functions
{
    /// <summary>
    /// log関数を表します。
    /// </summary>
    [Serializable()]
    public class Log : Function
    {
        /// <summary>
        /// 対数関数を初期化します。
        /// </summary>
        public Log() : base(1d, 10d) { }

        /// <summary>
        /// 対数関数を初期化します。
        /// </summary>
        /// <param name="f">対数の対象。</param>
        public Log(Formula f) : base(f, 10d) { }

        /// <summary>
        /// 対数関数を初期化します。
        /// </summary>
        /// <param name="f">対数の対象。</param>
        /// <param name="b">対数の底。</param>
        public Log(Formula f, Formula b) : base(f, b) { }

        /// <summary>
        /// 引数を指定して、関数を生成します。
        /// </summary>
        /// <param name="args">初期化に用いるか変数の数式。</param>
        /// <returns>初期化された関数。</returns>
        public override Function CreateFunction(params Formula[] args)
        {
            if (args.Length < 2) return new Log(args[0]);
            else return new Log(args[0], args[1]);
        }

        /// <summary>
        /// 対数の底を取得します。
        /// </summary>
        public Formula LogBase
        {
            get
            {
                if (Argument.Count > 1) return Argument[1];
                else return new Numeric(10d);
            }
        }

        public override Formula CalculateFunction()
        {
            Formula a = Argument[0];
            Formula b = Argument.Count > 1 ? Argument[1] : null;

            if (a == 1) return 0;
            if (a == b) return 1;

            Numeric an = a as Numeric;
            if (an != null && an > 0 && b == null) return new Numeric(an.Figure.Log(new Numeric(10d)));

            Numeric bn = b as Numeric;
            if (an != null && an > 0 && bn != null) return new Numeric(an.Figure.Log(bn));

            return this;
        }

        public override Formula this[int i]
        {
            get
            {
                if (i == 1 && base[i] == null) return new Numeric(10.0);
                return base[i];
            }
            set { base[i] = value; }
        }

        public override int MinimumArgumentQty
        {
            get { return 1; }
        }

        public override int MaximumArgumentQty
        {
            get { return 2; }
        }

        public override string GetInformation(out List<string> args)
        {
            args = new List<string>(); args.Add("数値"); args.Add("底（省略可。省略時10）");
            return "指定された数を底とする数値の対数を返します。";
        }

        /// <summary>
        /// 指定処理に関連するルールをすべて返す反復子を取得します。
        /// </summary>
        /// <param name="deformToken">変形識別トークン。</param>
        /// <param name="sender">ルール適用対象となる最上位親数式。</param>
        /// <param name="history">変形履歴情報。</param>
        /// <returns>変形に関連するルールを返す反復子。</returns>
        public override IEnumerable<Rule> GetRelatedRulesOf(DeformToken deformToken, Formula sender, DeformHistory history) 
        {
            foreach (var rule in base.GetRelatedRulesOf(deformToken, sender, history)) yield return rule;

            if (sender is Log && (deformToken.Has<NumerateToken>() || deformToken.Has<CalculateToken>()))
            {
                yield return LogOfImaginaryRule.Entity;
            }
            if ((deformToken.Has<CalculateToken>() || deformToken.Has<DifferentiateToken>()) && sender is Differentiate)
            {
                var x = new RulePatternVariable("x");
                var y = new RulePatternVariable("y");
                yield return new InstantPatternRule(new Differentiate(new Log(x, y), x), 1 / (x * new Ln(y)));

                yield return new DifferentiateCompositeFunctionRule(0);
            }
        }
    }
}
