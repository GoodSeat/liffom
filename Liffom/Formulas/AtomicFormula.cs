using System;
using System.Collections.Generic;
using System.Text;
using GoodSeat.Liffom.Formulas;

namespace GoodSeat.Liffom.Formulas
{
    /// <summary>
    /// 変数、関数など、主変数として扱うことのできる原子的数式を表します。
    /// </summary>
    [Serializable()]
    public abstract class AtomicFormula : Formula
    {

    }
}

